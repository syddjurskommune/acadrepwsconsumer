﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcadrePWSConsumer
{
    class Program
    {
        static void Main(string[] args)
        {
            var caseService = Acadre.AcadreServiceFactory.GetCaseService7();
            var contactService = Acadre.AcadreServiceFactory.GetContactService7();

            // I XML-data fra Acadre findes i dag et sags-ID (CaseFileReference), 
            string caseFileReference = "526427";

            var byggesag = caseService.GetCase(caseFileReference);

            var adresse = byggesag.CaseFileTitleText; // indeholder f.eks. "Kirkebakken 2, 8400 Ebeltoft"
            var sagsindhold = byggesag.CustomFields.df1; // indeholder f.eks. "Enfamiliehuse, større carporte, garager m.v."
            var matrikelnummerOgEjerlav = byggesag.CustomFields.df3; // indeholder f.eks. "1X, LYNGSBÆKGÅRD HGD., DRÅBY"
            var ejendomsnummer = byggesag.TitleAlternativeText; // indeholder f.eks. "Ejendom: 2508" (men kan i nogle tilfælde indeholde f.eks. "Matrikel: 9738 1L102"

            /* 
                Vi har ikke vejnummer i Acadre. Hvis i skal bruge det kan i evt. bruge DAWA http://dawa.aws.dk/
                eks. tag adresse fra CaseFileTitleText og lav dette kald:
                http://dawa.aws.dk/adresser/autocomplete?q=Kirkebakken%202,%208400%20Ebeltoft
                Læs adresse->id fra resultatet og lav dette kald:
                http://dawa.aws.dk/adresser/0a3f50bf-7ae7-32b8-e044-0003ba298018
                Læs alt muligt om adressen (det er muligt at det er nmmere at læse nogle af de øvrige oplysninger herfra også (matrikel, lav osv.)               
            */

            // Øvrige parter
            foreach (var partRef in byggesag.Party.Where(p => !p.IsPrimary))
            {
                var part = contactService.GetContact(partRef.ContactReference);
                // Hent noget adresse ud fra part
                // Den kan være i forskellige strukturer (jf. OIO) alt efter om det er Fima, person eller ejendom
                // og den kan muligvis også have afvigende struktur alt efter om det er en dansk eller en udenlandsk adresse.
                // I bør nok finde en række repræsentative sager med forskellige partstyper og se på de faktiske data i disse for at få de fleste cases med.
            }

            // Hvis man KUN vil have parter angivet med rollen "Sekundær" kan man lave noget som det her istedet:
            // foreach (var party in byggesag.Party.Where(p => p.PartyRelationTypeLiteral=="Sekundær"))




        }
    }
}
