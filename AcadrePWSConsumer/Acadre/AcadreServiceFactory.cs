﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Services.Protocols;

namespace AcadrePWSConsumer.Acadre
{
    public static class AcadreServiceFactory
    {
        private static AcadreServiceV7.CaseService7 caseService7;
        private static AcadreServiceV7.ContactService7 contactService7;


        public static AcadreServiceV7.CaseService7 GetCaseService7()
        {
            if (caseService7 == null)
            {
                caseService7 = new AcadreServiceV7.CaseService7();
                ConfigureService(caseService7);
            }
            return caseService7;
        }

        public static AcadreServiceV7.ContactService7 GetContactService7()
        {
            if (contactService7 == null)
            {
                contactService7 = new AcadreServiceV7.ContactService7();
                ConfigureService(contactService7);
            }
            return contactService7;
        }

        private static void ConfigureService(SoapHttpClientProtocol service)
        {
            // Disse credentials er dem som bruges til at tilgå pws service ressourcen på IIS
            service.Credentials = new System.Net.NetworkCredential(
                Properties.Settings.Default.PWSServiceUserName
                , Properties.Settings.Default.PWSServiceUserPassword
                , Properties.Settings.Default.PWSServiceUserDomain);

            // Dette brugernavn er det som bruges til at autentificere og autorisere slutbrugeren over for Acadre
            // Det kan være det samme som ovenstående credentials hvis man vil køre det hele som en system-adgang.
            Acadre.PWSHeaderExtension.User = Identity.User.GetUserName();
        }
    }
}
